# Contact Manager

## Features to implement

 - [x] List Contacts
 - [x] View Contact
 - [x] Add Contact
 - [x] Delete Contact
 - [x] Edit Contact
 - [x] Clean routing i.e '/contact/:id'
 - [x] Use material-ui for components

### Bonuses

- [ ] Use [xstate](https://xstate.js.org/docs) to manage app state.
- [x] Connect to the graphql endpoint http://localhost:3001 by using create-react-app proxy feature
- [x] Use the graphql endpoint to get/create/update/delete

### Screenshots

#### Landing page
![image](https://user-images.githubusercontent.com/55896/75116852-528e5780-5664-11ea-9d2c-45a827b292c6.png)

#### Details page
![image](https://user-images.githubusercontent.com/55896/75116825-0b07cb80-5664-11ea-89fb-68f63879f58e.png)

#### Edit mode
![image](https://user-images.githubusercontent.com/55896/75116834-207cf580-5664-11ea-9dde-0198c0e6fd46.png)

------------------------------------------------------------------------

### Contact

- Name eg 'John Smith'
- Email eg 'john@smith.com'
- Date Modified eg '31-01-2018 15:04'
- Date Created eg '31-01-2018 15:04'

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run gql`

Runs the graphql server.

Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
