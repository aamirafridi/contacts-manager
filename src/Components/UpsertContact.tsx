import { ADD_CONTACT, UPDATE_CONTACT } from "../gql/mutation";
import { Button, TextField } from "@material-ui/core";
import React, {
  FunctionComponent,
  memo,
  useCallback,
  useEffect,
  useState
} from "react";

import AddIcon from "@material-ui/icons/Add";
import CancelIcon from "@material-ui/icons/Close";
import { Contact } from "../react-app-env";
import EditIcon from "@material-ui/icons/Edit";
import { useMutation } from "@apollo/react-hooks";

type Props = {
  onUpsertContact: () => void;
  onCancel?: () => void;
  contact?: Contact | null;
};

const blankContact = Object.freeze({ id: "", name: "", email: "" });

const UpsertContact: FunctionComponent<Props> = memo(props => {
  const { onUpsertContact, onCancel = () => {}, contact } = props;
  const [addContact] = useMutation(ADD_CONTACT);
  const [updateContact] = useMutation(UPDATE_CONTACT);
  const [formValues, setFormValues] = useState(blankContact);
  const focusName = () => {
    const nameInput = document.getElementById("name");
    if (nameInput) {
      nameInput.focus();
    }
  };

  useEffect(() => {
    setFormValues(contact || blankContact);
    focusName();
  }, [contact]);

  const updateValue = useCallback(e => {
    e.persist();
    setFormValues(formValues => ({
      ...formValues,
      [e.target.name]: e.target.value
    }));
  }, []);

  const submitHandler = useCallback(
    e => {
      e.preventDefault();
      formValues.id
        ? updateContact({ variables: formValues })
        : addContact({ variables: formValues });
      setFormValues(blankContact);
      focusName();
      onUpsertContact();
    },
    [addContact, formValues, onUpsertContact, updateContact]
  );

  const legendCopy = contact ? `Updating: ${formValues.name}` : "New contact";
  const btnLabel = contact ? "Update" : "Add";
  const btnIcon = contact ? <EditIcon /> : <AddIcon />;

  return (
    <form onSubmit={submitHandler} className="form">
      <fieldset>
        <legend>{legendCopy}</legend>
        <TextField
          required
          name="name"
          label="Name"
          id="name"
          onChange={updateValue}
          value={formValues.name}
        />
        <TextField
          required
          type="email"
          name="email"
          label="Email"
          id="email"
          onChange={updateValue}
          value={formValues.email}
        />
        <Button
          type="submit"
          variant="contained"
          color="primary"
          data-testid="assertBtn"
          startIcon={btnIcon}
        >
          {btnLabel}
        </Button>
        {contact && (
          <Button
            onClick={() => {
              onCancel();
              setFormValues(blankContact);
            }}
            data-testid="cancelBtn"
            variant="contained"
            color="secondary"
            startIcon={<CancelIcon />}
          >
            Cancel
          </Button>
        )}
      </fieldset>
    </form>
  );
});

export default UpsertContact;
