import {
  IconButton,
  LinearProgress,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText
} from "@material-ui/core";
import React, { FunctionComponent, useState } from "react";

import { Alert } from "@material-ui/lab";
import { Contact } from "../react-app-env";
import { GET_CONTACTS } from "../gql/query";
import { Link } from "react-router-dom";
import SettingsIcon from "@material-ui/icons/Settings";
import UpsertContact from "./UpsertContact";
import { useQuery } from "@apollo/react-hooks";

type ContactsData = {
  contacts: Array<Contact>;
};

const ContactsList: FunctionComponent = () => {
  const { loading, error, data, refetch } = useQuery<ContactsData>(
    GET_CONTACTS,
    {
      fetchPolicy: "no-cache"
    }
  );

  if (loading)
    return <LinearProgress variant="query" aria-busy role="progressbar" />;
  if (error) return <Alert severity="error">Ops! Something went wrong!</Alert>;

  return (
    <>
      <UpsertContact onUpsertContact={refetch} />
      {!data || data.contacts.length === 0 ? (
        <Alert severity="info">No contacts found.</Alert>
      ) : (
        <List>
          {data.contacts.map(contact => (
            <ListItem key={contact.id}>
              <ListItemText primary={contact.name} secondary={contact.email} />
              <ListItemSecondaryAction>
                <Link to={`/${contact.id}`}>
                  <IconButton title="Manage this contact">
                    <SettingsIcon />
                  </IconButton>
                </Link>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      )}
    </>
  );
};

export default ContactsList;
