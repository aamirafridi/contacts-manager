import {
  Button,
  Card,
  CardActions,
  CardContent,
  LinearProgress,
  Link,
  Typography
} from "@material-ui/core";
import React, { FunctionComponent, useState } from "react";
import { Link as RouterLink, useHistory, useParams } from "react-router-dom";

import { Alert } from "@material-ui/lab";
import { Contact } from "../react-app-env";
import DeleteContact from "./DeleteContact";
import EditIcon from "@material-ui/icons/Edit";
import { GET_CONTACT } from "../gql/query";
import UpsertContact from "./UpsertContact";
import { useQuery } from "@apollo/react-hooks";

type ContactData = {
  contact: Contact;
};

type ContactVars = {
  id?: string;
};

const ContactDetails: FunctionComponent = () => {
  const { id } = useParams();
  const { push } = useHistory();
  const { loading, error, data, refetch } = useQuery<ContactData, ContactVars>(
    GET_CONTACT,
    { variables: { id } }
  );
  const [isEdit, setIsEdit] = useState<boolean>(false);
  if (loading)
    return <LinearProgress variant="query" aria-busy role="progressbar" />;
  if (error) return <Alert severity="error">Ops! Something went wrong!</Alert>;

  return !data || !data.contact ? (
    <Alert severity="info">No contact found.</Alert>
  ) : (
    <>
      {isEdit && (
        <UpsertContact
          onUpsertContact={() => {
            refetch();
            setIsEdit(false);
          }}
          contact={data.contact}
          onCancel={() => setIsEdit(false)}
        />
      )}
      <Card className="contact-card" raised>
        <CardContent className="vcard">
          <Typography className="fn" align="center" variant="h2" gutterBottom>
            {data.contact.name}
          </Typography>
          <Typography
            className="email"
            align="center"
            color="textSecondary"
            gutterBottom
          >
            <a
              href={`mailto:${data.contact.email}?Subject=Hello%20again`}
              target="_blank"
              rel="noopener noreferrer"
            >
              {data.contact.email}
            </a>
          </Typography>
        </CardContent>
        <CardActions className="actions">
          <Button
            onClick={() => setIsEdit(true)}
            size="large"
            variant="contained"
            color="primary"
            startIcon={<EditIcon />}
          >
            Edit
          </Button>
          <DeleteContact id={data.contact.id} onDelete={() => push("/")} />
        </CardActions>
      </Card>
      <Link component={RouterLink} to="/" className="back-link">
        Return to the list
      </Link>
    </>
  );
};

export default ContactDetails;
