import React, { FunctionComponent, memo, useCallback } from "react";

import { Button } from "@material-ui/core";
import { DELETE_CONTACT } from "../gql/mutation";
import DeleteIcon from "@material-ui/icons/DeleteForever";
import { useMutation } from "@apollo/react-hooks";

type Props = {
  onDelete: Function;
  id: string;
};

const DeleteContact: FunctionComponent<Props> = memo(props => {
  const { onDelete, id } = props;
  const [deleteContact] = useMutation(DELETE_CONTACT);

  const deleteHandler = useCallback(
    e => {
      e.preventDefault();
      if (window.confirm("Are you sure you want to delete this contact?")) {
        deleteContact({ variables: { id } });
        onDelete();
      }
    },
    [deleteContact, onDelete]
  );

  return (
    <Button
      onClick={deleteHandler}
      color="default"
      size="large"
      variant="contained"
      startIcon={<DeleteIcon />}
    >
      Delete
    </Button>
  );
});

export default DeleteContact;
