import { render, wait } from "@testing-library/react";

import ContactsList from "../ContactsList";
import { GET_CONTACTS } from "../../gql/query";
import { MemoryRouter } from "react-router-dom";
import { MockedProvider } from "@apollo/react-testing";
import React from "react";

describe("ContactsList App", () => {
  it("should render correct elements", async () => {
    const mocks = [
      {
        request: {
          query: GET_CONTACTS
        },
        result: {
          data: {
            contacts: [{ id: "1", name: "Joe", email: "joe@gmail.com" }]
          }
        }
      }
    ];

    const { getByRole, getByText, getByLabelText, getByTitle } = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MemoryRouter>
          <ContactsList />
        </MemoryRouter>
      </MockedProvider>
    );

    // loading
    expect(getByRole("progressbar")).toBeInTheDocument();

    await wait();

    // contacts list
    expect(getByText(/Joe/)).toBeInTheDocument();
    expect(getByText(/joe@gmail.com/)).toBeInTheDocument();

    // actions buttons
    expect(getByTitle(/Manage this contact/)).toBeInTheDocument();

    // form to add new contact
    expect(getByLabelText(/Name/)).toBeInTheDocument();
    expect(getByLabelText(/Email/)).toBeInTheDocument();
  });
});
