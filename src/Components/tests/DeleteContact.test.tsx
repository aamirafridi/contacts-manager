import { fireEvent, render } from "@testing-library/react";

import DeleteContact from "../DeleteContact";
import React from "react";

const mockDeleteContact = jest.fn();

jest.mock("@apollo/react-hooks", () => ({
  useMutation: () => {
    const deleteContact = mockDeleteContact;
    return [deleteContact];
  }
}));

describe("Delete contact", () => {
  it("should delete a contact", async () => {
    const onDeleteMock = jest.fn();
    const { getByText } = render(
      <DeleteContact id={"1"} onDelete={onDeleteMock} />
    );

    window.confirm = jest.fn(() => true);
    fireEvent.click(getByText(/Delete/));

    expect(mockDeleteContact).toHaveBeenCalledWith({ variables: { id: "1" } });
    expect(onDeleteMock).toHaveBeenCalled();
  });
});
