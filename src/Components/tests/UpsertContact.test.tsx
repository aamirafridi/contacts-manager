import { fireEvent, render, wait } from "@testing-library/react";

import React from "react";
import UpsertContact from "../UpsertContact";

const mockAddContact = jest.fn();
const mockUpdateContact = jest.fn();

jest.mock("@apollo/react-hooks", () => ({
  useMutation: () => {
    const addContact = mockAddContact;
    const updateContact = mockUpdateContact;
    return [addContact, updateContact];
  }
}));

afterEach(jest.clearAllMocks);

describe("Add contact", () => {
  it("should add a new contact", async () => {
    const onUpsertContactMock = jest.fn();
    const onCancelMock = jest.fn();
    const { getByLabelText, getByTestId } = render(
      <UpsertContact
        onUpsertContact={onUpsertContactMock}
        onCancel={onCancelMock}
        contact={null}
      />
    );

    fireEvent.change(getByLabelText(/Name/), { target: { value: "Joe" } });
    fireEvent.change(getByLabelText(/Email/), {
      target: { value: "joe@gmail.com" }
    });
    fireEvent.click(getByTestId("assertBtn"));

    expect(mockAddContact).toHaveBeenCalledWith({
      variables: { email: "joe@gmail.com", id: "", name: "Joe" }
    });
  });
});

describe("Update contact", () => {
  it("should update current contact", async () => {
    const onUpsertContactMock = jest.fn();
    const onCancelMock = jest.fn();
    const { getByLabelText } = render(
      <UpsertContact
        onUpsertContact={onUpsertContactMock}
        onCancel={onCancelMock}
        contact={{
          id: "1",
          email: "joe@gmail.com",
          name: "Joe"
        }}
      />
    );

    await wait();

    expect(getByLabelText(/Name/)).toHaveValue("Joe");
    expect(getByLabelText(/Email/)).toHaveValue("joe@gmail.com");
    fireEvent.change(getByLabelText(/Name/), {
      target: { value: "Joe Bloggs" }
    });
    expect(getByLabelText(/Name/)).toHaveValue("Joe Bloggs");
  });
});
