import { fireEvent, render, wait } from "@testing-library/react";

import ContactDetails from "../ContactDetails";
import { GET_CONTACT } from "../../gql/query";
import { MemoryRouter } from "react-router-dom";
import { MockedProvider } from "@apollo/react-testing";
import React from "react";

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useParams: jest.fn(() => ({ id: "1" }))
}));

describe("ContactDetails App", () => {
  it("should render correct elements actions work", async () => {
    const mocks = [
      {
        request: {
          query: GET_CONTACT,
          variables: {
            id: "1"
          }
        },
        result: {
          data: {
            contact: { id: "1", name: "Joe", email: "joe@gmail.com" }
          }
        }
      }
    ];

    const { getByRole, getByText, getByLabelText, queryByLabelText } = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MemoryRouter>
          <ContactDetails />
        </MemoryRouter>
      </MockedProvider>
    );

    // loading
    expect(getByRole("progressbar")).toBeInTheDocument();

    await wait();

    // contact details
    expect(getByText(/Joe/)).toBeInTheDocument();
    expect(getByText(/joe@gmail.com/)).toBeInTheDocument();

    // actions buttons
    expect(getByText(/Edit/)).toBeInTheDocument();
    expect(getByText(/Delete/)).toBeInTheDocument();

    // show update form
    fireEvent.click(getByText(/Edit/));
    expect(getByLabelText(/Name/)).toHaveValue("Joe");
    expect(getByLabelText(/Email/)).toHaveValue("joe@gmail.com");
    expect(getByText(/Update/)).toBeInTheDocument();
    expect(getByText(/Cancel/)).toBeInTheDocument();

    // hide form
    fireEvent.click(getByText(/Cancel/));
    expect(queryByLabelText(/Name/)).not.toBeInTheDocument();

    // back to list link
    fireEvent.click(getByText(/Return to the list/));
    await wait(() => expect(window.location.href).toMatch(/\//));
  });
});
