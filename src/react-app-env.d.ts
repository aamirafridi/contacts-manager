/// <reference types="react-scripts" />

export type Contact = {
  id: string;
  name: string;
  email: string;
};
