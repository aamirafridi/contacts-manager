import { createMuiTheme } from "@material-ui/core/styles";
import deepOrange from "@material-ui/core/colors/deepOrange";
import lightBlue from "@material-ui/core/colors/lightBlue";

export const hot = createMuiTheme({
  palette: {
    primary: deepOrange,
    secondary: lightBlue
  }
});
