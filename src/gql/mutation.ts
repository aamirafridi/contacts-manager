import { gql } from "apollo-boost";

export const ADD_CONTACT = gql`
  mutation AddContact($name: String!, $email: String!) {
    addContact(contact: { name: $name, email: $email }) {
      id
      name
      email
    }
  }
`;

export const DELETE_CONTACT = gql`
  mutation DeleteContact($id: ID!) {
    deleteContact(id: $id)
  }
`;

export const UPDATE_CONTACT = gql`
  mutation UpdateContact($id: ID!, $name: String, $email: String) {
    updateContact(contact: { id: $id, name: $name, email: $email }) {
      id
      name
      email
    }
  }
`;
