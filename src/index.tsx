import "./index.css";

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import App from "./App";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";
import { hot } from "./themes";

const apolloClient = new ApolloClient({
  uri: "/api"
});

ReactDOM.render(
  <ApolloProvider client={apolloClient}>
    <Router>
      <ThemeProvider theme={hot}>
        <App />
      </ThemeProvider>
    </Router>
  </ApolloProvider>,
  document.getElementById("root")
);
