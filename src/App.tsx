import React, { FunctionComponent } from "react";
import { Route, Switch } from "react-router-dom";

import ContactDetails from "./Components/ContactDetails";
import ContactsList from "./Components/ContactsList";
import { Container } from "@material-ui/core";

const App: FunctionComponent = () => (
  <Container maxWidth="md">
    <h1>Contacts Manager</h1>
    <Switch>
      <Route exact path="/" component={ContactsList} />
      <Route exact path="/:id" component={ContactDetails} />
    </Switch>
  </Container>
);

export default App;
